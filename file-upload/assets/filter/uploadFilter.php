<?php

// The user may only upload a file with a size under 3MB:
if ($_FILES["image"]["size"] < 3000000) {

    // The user may only upload .gif or .jpeg files
    $acceptedFileTypes = ["image/gif", "image/jpg", "image/jpeg", "image/png"];

    // retrieve the MIME type of the uploaded file
    $uploadedFileType = finfo_file(finfo_open(FILEINFO_MIME_TYPE), $_FILES["image"]["tmp_name"]);

    // If the MIME type is in the array, proceed
    if (in_array($uploadedFileType, $acceptedFileTypes)) {
        $imageName = $_FILES["image"]["name"];
        if (strlen(trim($imageName)) <= 50) {
            if (checkForUpper($imageName)) {
                if ($_FILES["image"]["error"] > 0) {
                    echo "Error: " . $_FILES["image"]["error"] . "<br />";
                } else {
                    $extension = getExtension($uploadedFileType);
                    moveUploadedFile($_FILES["image"], $extension);
                    header("Location: ../../index.php");
                }
            } else {
                echo "Atleast 1 capital letter is needed to upload the file!";
            }
        } else {
            echo "Your name contains to much characters! 50 or less characters are required to upload a file.";
        }
    } else {
        echo "Invalid file type. Must be gif, jpg, jpeg or png.";
    }
} else {
    echo "Invalid file size. Must be less than 60kb.";
}

// Move an uploaded file to the correct map, based on MIME type
function moveUploadedFile($file, $extension) {
  if (file_exists("../../uploads/" . $extension . "/" . hash("md5", $file["name"]). "." . $extension)){
    echo "File " . $file["name"] . " already exists!" ;
    exit(1);
  } else {
    if (move_uploaded_file($file["tmp_name"], "../../uploads/" . $extension . "/" . hash("md5", $file["name"]). "." . $extension)) {
      echo "Image has been stored!";
    } else {
      echo "ERROR storing image!";
      exit(1);
    }
  }
}

// retrieve the correct extension, based on MIME type
function getExtension($mimeType) {
  $extension = "";
  switch ($mimeType) {
    case 'image/gif':
      $extension = "gif";
      break;
    case 'image/jpg':
      $extension = "jpg";
      break;
    case 'image/jpeg':
      $extension = "jpeg";
      break;
    case 'image/png':
      $extension = "png";
      break;         
    default:
      echo "No valid MIME type was found! Please get back to the main page!";
      exit(1);
      break;
  }
  return $extension;
}

// Checks if a string contains atleast 1 uppercase character
function checkForUpper($string) {
    for ($character = 0; $character < strlen($string); $character++) {
        if (ctype_upper($string[$character])) {
            return true;
        }
    }
    return false;
}
