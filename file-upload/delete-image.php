<?php 

if (isset($_POST['submit'])){
  $image = $_POST["submit"]; 
  $extension = explode(".", $image)[1];
  if (checkExtension($extension, array("gif", "png", "jpg", "jpeg"))){
    if (checkAndDelete($image, $extension)){
      echo "Image deleted!";
      header("Location: overzicht.php?extension=" . $extension);
    } else {
      echo "ERROR deleting the file!";
    }
  } else {
    echo "ERROR: invalid extension found or the image has multiple dots! (.)";
  }
}

// checks if the extension is equal to one of the allowed extensions
function checkExtension($extension, $allowedExtensions) {
  for ($i=0; $i < count($allowedExtensions); $i++) { 
    if ($allowedExtensions[$i] == $extension){
      return true;
    }
  }
  return false;
}

// checks if an image exists in the directory, if so, it will delete the image
function checkAndDelete($imageName, $extension){
  $imagePath = "uploads/".$extension."/".$imageName;
  if (file_exists($imagePath)){
    unlink($imagePath); 
    return true;
  }
  return false;
}
?>