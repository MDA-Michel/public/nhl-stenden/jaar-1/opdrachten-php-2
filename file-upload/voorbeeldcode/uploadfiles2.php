<?php

//#### Upload with restrictions ####

    //The user may only upload a file with a size under 60 kb:
    if ($_FILES["uploadedFile"]["size"] < 60000)
    {
        //The user may only upload gif or .jpeg files 
        $acceptedFileTypes = ["image/gif", "image/jpg", "image/jpeg"];
        $fileinfo = finfo_open(FILEINFO_MIME_TYPE);
        $uploadedFileType = finfo_file($fileinfo, $_FILES["uploadedFile"]["tmp_name"]);

        //A shorter version of line 9 - 11
        //$uploadedFileType = finfo_file(finfo_open(FILEINFO_MIME_TYPE), $_FILES["uploadedFile"]["tmp_name"]);

        //If the type is in the array, proceed
        if(in_array($uploadedFileType, $acceptedFileTypes))
        {
            if ($_FILES["uploadedFile"]["error"] > 0)
            {
                echo "Error: " . $_FILES["uploadedFile"]["error"] . "<br />";
            } else {
                echo "Upload: " . $_FILES["uploadedFile"]["name"] . "<br />";
                echo "Type: " . $uploadedFileType . "<br />";
                echo "Size: " . ($_FILES["uploadedFile"]["size"] / 1024) . " Kb<br />";
                echo "Stored in: " . $_FILES["uploadedFile"]["tmp_name"];
            }
        }else{
            echo "Invalid file type. Must be gif, jpg or jpeg.";
        }
    } else
    {
        echo "Invalid file size. Must be less than 60kb.";
    }
?> 