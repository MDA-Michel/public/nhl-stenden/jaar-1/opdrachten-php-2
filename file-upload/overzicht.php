<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="./assets/css/style.css">
  <title>Document</title>
</head>

  <body>
    <?php
      if (isset($_GET['extension'])) {
          $imageIntro = "Welcome to the '" . $_GET['extension'] . "' image Gallery!";

          // create the path to the upload directory
          $path = './uploads/' . $_GET["extension"] . "/";

          // retrieve the files from the correct image directory, from this point '$files' is an array if images are present.
          $files = array_diff(scandir($path), array('.', '..'));

          switch ($_GET["extension"]) {
              case 'gif':
                  echo "<div class=\"image-container\">";
                    echo "<h1> $imageIntro </h1>";
                    echo "<p> Click <a href=\"index.php\"> here </a> to return to the upload image page</p>";
                    generateImages($files, $path, "gif");
                  echo "</div>";
                  break;
              case 'png':
                  echo "<div class=\"image-container\">";
                    echo "<h1> $imageIntro </h1>";
                    echo "<p> Click <a href=\"index.php\"> here </a> to return to the upload image page</p>";
                    generateImages($files, $path, "png");
                  echo "</div>";
                  break;
              case 'jpg':
                  echo "<div class=\"image-container\">";
                    echo "<h1> $imageIntro </h1>";
                    echo "<p> Click <a href=\"index.php\"> here </a> to return to the upload image page</p>";
                    generateImages($files, $path, "jpg");
                  echo "</div>";
                  break;
              case 'jpeg':
                  echo "<div class=\"image-container\">";
                    echo "<h1> $imageIntro </h1>";
                    echo "<p> Click <a href=\"index.php\"> here </a> to return to the upload image page</p>";
                    generateImages($files, $path, "jpeg");
                  echo "</div>";
                  break;
              default:
                  echo "<p> Not a valid extension! Please return to the <a href=\"index.php\"> upload page </a></p>";
                  break;
          }
      } else {
          echo "<p> Please return to the <a href=\"index.php\"> upload page </a> </p>";
      }
    ?>
  </body>
</html>

<?php

// generate all images from a directory dynamically on the web page.
function generateImages($files, $path, $type) {
    foreach ($files as $key => $image) {
        $imagePath = $path . "/" . $image;
        echo "<img src=\"$imagePath\" alt=\"" . $type . " image\" width=\"400px\" height=\"400px\">" . PHP_EOL;
        echo "<form method=\"POST\" action=\"delete-image.php\">";
          echo "<button type=\"submit\" name=\"submit\" value=\"$image\"> verwijder deze afbeelding </button>" . PHP_EOL;
        echo "</form>";
    }
}

?>