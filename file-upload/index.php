<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>upload</title>
  <link rel="stylesheet" href="./assets/css/style.css">
</head>

<body class="body-index">

  <div class="container">
    <div>
      <p>
      <h1> Hamburgers met korting, ohooohohohoh </h1>
      <small>.gif, .jpg, .jpeg or .png image only</small>
      </p>
      <form action="./assets/filter/uploadFilter.php" method="POST" enctype="multipart/form-data">
        <div>
          <label for="uploaded-image"> upload jouw favoriete hamburger! </label>
          <input type="file" name="image" id="uploaded-image">
        </div>
        <input type="submit" name="submit" value="upload die burger">
      </form>
    </div>
    <?php 
    
    $extensions = array("gif", "png", "jpg", "jpeg");
    echo "<ul>";
    for ($i=0; $i < count($extensions) ; $i++) { 
      echo "<li>Ga door naar je favoriete <a href=\"./overzicht.php?extension=". $extensions[$i] ."\">".$extensions[$i]." hamburger</a> pagina!</li>".PHP_EOL;
    }
    echo "</ul>"
    ?> 
  </div>

</body>

</html>