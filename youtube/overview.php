<!DOCTYPE html>
<html lang="en">

  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="assets/css/style.css" type="text/css">
    <?php include "assets/database/database.php"?>
  </head>
    
  <body>
      <?php 
      
      $videoRecords = getTableRecords("SELECT * FROM video"); 
      // print_r($videoRecords);
      echo "<table>";
        echo "<tr>";
          echo "<td class=\"table-head\"> # </td>";
          echo "<td class=\"table-head\"> artist </td>";
          echo "<td class=\"table-head\"> Songtitle </td>";
          echo "<td class=\"table-head\"> Play video </td>";
          echo "<td class=\"table-head\"> Delete video</td>";
        echo "</tr>";
      for ($row=0; $row < count($videoRecords); $row++) { 
        echo "<tr>";
          echo "<td>" . $row + 1 ."</td>";
          echo "<td>" . $videoRecords[$row]["artist"] ."</td>";
          echo "<td>" . $videoRecords[$row]["songtitle"] ."</td>";
          echo "<td> <a href=\"playVideo.php?id=". $videoRecords[$row]["id"] ."\"> Play </a> </td>";
          echo "<td> <a href=\"deleteVideo.php?id=". $videoRecords[$row]["id"] ."\"> Delete </a> </td>";
        echo "</tr>";
      }
      echo "</table>"
      ?>
  <p> add a new <a href="addVideo.php">video</a> </p>
  </body>

</html>