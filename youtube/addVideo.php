<!DOCTYPE html>
<html lang="en">

  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="assets/css/style.css" type="text/css">
    <?php include "assets/database/database.php"?>
  </head>

  <body>
  <div>
    <form action="<?php echo htmlentities($_SERVER["PHP_SELF"])?>" method="POST">
      <div>
        <label for="artist">Artist</label>
        <input type="text" id="artist" name="artist" required>
      </div>
      <div>
        <label for="id">Video ID</label>
        <input type="text" id="id" name="id" required>
      </div>
      <div>
        <label for="songtitle">Songtitle</label>
        <input type="text" id="songtitle" name="songtitle" required>
      </div>
      <div>
        <input type="submit" name="submit" value="Submit">
      </div>
    </form>
  </div>
  </body>

    <?php 
    
    
  if (isset($_POST['submit'])){
    $values["id"] = !empty($_POST['id']) ? FILTER_INPUT(INPUT_POST, 'id', FILTER_SANITIZE_SPECIAL_CHARS) : false;
    $values["artist"] = !empty($_POST['artist']) ? FILTER_INPUT(INPUT_POST, 'artist', FILTER_SANITIZE_SPECIAL_CHARS) : false;
    $values["songtitle"] = !empty($_POST['songtitle']) ? FILTER_INPUT(INPUT_POST, 'songtitle', FILTER_SANITIZE_SPECIAL_CHARS) : false;

    foreach ($values as $key => $value) {
      if (!$value){
        echo "<p> <strong>$key</strong> field has no value! Please fill in the value </p>";
      } 
    }

    if (!in_array(false, $values)){
      executeQuery("INSERT INTO video (id, artist, songtitle) VALUES (?, ?, ?)", $values, "insert");
      header("Location: overview.php");
    } 
  }

    ?>
</html>