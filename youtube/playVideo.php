<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="assets/css/style.css" type="text/css">
    <?php include "assets/database/database.php" ?>
</head>

<body>
<div class="container">
    <?php

    if (!empty($_GET["id"])) {
        $record = getTableRecord("SELECT * FROM video WHERE id = ?", $_GET['id']);
        if (!empty($record)) {
            echo "<p>";
            echo "<h1>" . $record["songtitle"] . "</h1>";
            echo "made by " . $record["artist"];
            echo "</p> ";
            echo "<iframe src=\"https://www.youtube.com/embed/" . $record["id"] . "\" width=\"800\" height=\"600\"></iframe>";
            echo "<p style=\"margin-top: 25px\">" . $record["description"] . "</p>";
            echo "<p> Return the the <a href=\"overview.php\"> Mainpage </a></p>";
        } else {
            echo "<p> Unable to find record! </p>";
            echo "<p> Return the the <a href=\"overview.php\"> Mainpage </a></p>";
        }
    } else {
        echo "<p> No ID has been set! please return the the <a href=\"overview.php\"> Mainpage </a></p>";
    }

    ?>
</div>
</body>

</html>

