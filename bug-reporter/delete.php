<?php
  include "assets/database/database.php";
  
  if (isset($_GET["id"])){
    $value["id"] = $_GET['id'];
    executeQuery("DELETE FROM bug WHERE id = ?", $value, "delete");
    header("Location: mainpage.php");
  } else {
    echo "<p>no ID has been specified</p>";
  }
?>