<?php 

$db = dbConnect("localhost", "root", "root", "jaar_1_periode_2");

// connect to the database, return the $conn var if the connection is succesful, if not return an error message
function dbConnect($server, $username, $password, $database){
    // check if the connection exists
    $conn = mysqli_connect($server, $username, $password, $database);
    if (!$conn) {
      exit("Could not connect to the database!");
    } 
    return $conn;
}

// execute a select query on the database and return the results, this retrieves multiple records
function getTableRecords($sql){
  $db = dbConnect("localhost", "root", "root", "jaar_1_periode_2");  
  $stmt = mysqli_prepare($db, $sql) or die( mysqli_stmt_error($stmt) );
  mysqli_stmt_execute($stmt) or die( mysqli_stmt_error($stmt) );
  $result = mysqli_stmt_get_result($stmt) or die( mysqli_stmt_error($stmt));
  $rows = array();
  while($row = mysqli_fetch_assoc($result)) {
    array_push($rows, $row);
  }
  mysqli_stmt_close($stmt);
  mysqli_close($db);
  return $rows;
}

// execute a select query on the database and return the results, this retrieves a single record
function getTableRecord($sql, $id){
  global $db;
  $stmt = mysqli_prepare($db, $sql) or die( mysqli_stmt_error($stmt) );
  mysqli_stmt_bind_param($stmt, 'i', $id) or die( mysqli_stmt_error($stmt) );
  mysqli_stmt_execute($stmt) or die( mysqli_stmt_error($stmt) );
  $result = mysqli_stmt_get_result($stmt);
  $row = mysqli_fetch_assoc($result);
  mysqli_stmt_close($stmt);
  mysqli_close($db);
  return $row;
}

// execute a update, insert or delete query on the bug table and return true or false (), used for the 
function executeQuery($sql, $values, $type) {
  $localConn =  dbConnect("localhost", "root", "root", "jaar_1_periode_2");
  $stmt = mysqli_prepare($localConn, $sql);
  // print_r($values);
  switch ($type) {
    case "delete":
      mysqli_stmt_bind_param($stmt, 'i', $values["id"]);
      break;
    case "insert":
      mysqli_stmt_bind_param($stmt, 'ssssss', $values["product_name"], $values["version"], $values["hardware_type"], $values["os"], $values["frequency"], $values["solution"]);
      break;
    case "update":
      mysqli_stmt_bind_param($stmt, 'ssssssi', $values["product_name"], $values["version"], $values["hardware_type"], $values["os"], $values["frequency"], $values["solution"], $values["id"]);
      break;
    default:
      echo "Query could not be executed. Please return to the <a href=\"./mainpage.php\"> Mainpage</a>";
      break;
  }

  mysqli_stmt_execute($stmt);
  mysqli_close($localConn);

}

?>