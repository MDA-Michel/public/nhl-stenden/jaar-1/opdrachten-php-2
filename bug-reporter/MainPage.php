<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Mainpage</title>
  <link rel="stylesheet" href="assets/css/style.css">
  <?php include "assets/database/database.php"?>
</head>

<body>
  <h1>Bugreporter</h1>
  <div>
    <table>
      <tr>
        <td class="table-head"> # </td>
        <td class="table-head">Product Name</td>
        <td class="table-head">Version</td>
        <td class="table-head">Hardware Type</th>
        <td class="table-head">OS</>
        <td class="table-head">Frequency</td>
        <td class="table-head">Solution</td>
        <td class="table-head">Edit item </td>
        <td class="table-head">Delete item</td>
      </tr>

      <?php 
      $tableData = getTableRecords("SELECT * FROM bug");
      // print_r($tableData);
      // print all the data that is in the bug table
      for ($row=0; $row < count($tableData); $row++) { 
        echo "<tr>";
          echo "<td> " . $row + 1 . " </td>";
          echo "<td> " . $tableData[$row]["product_name"] . " </td>";
          echo "<td> " . $tableData[$row]["version"] . " </td>";
          echo "<td> " . $tableData[$row]["hardware_type"] . " </td>";
          echo "<td> " . $tableData[$row]["os"] . " </td>";
          echo "<td> " . $tableData[$row]["frequency"] . " </td>";
          echo "<td> " . $tableData[$row]["solution"] . " </td>";
          echo "<td> 
                  <a href='./Edit.php?id=" . $tableData[$row]["id"] . "'> Edit </a> 
                </td>".PHP_EOL;
          echo "<td> 
                  <a href='./delete.php?id=" . $tableData[$row]["id"] . "'> Delete </a> 
                </td>".PHP_EOL;
        echo "</tr>".PHP_EOL;
      }
      ?>

    </table>
    <p> <a href="addbug.php">add a new bug</a> </p>

  </div>
</body>

</html>