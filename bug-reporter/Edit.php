<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Edit.php</title>
  <link rel="stylesheet" href="assets/css/style.css"> 
  <?php include "assets/database/database.php"?>
</head>

<body>
  <h1>Edit bug</h1>
  <?php if (!empty($_GET["id"])):
      $recordData = getTableRecord("SELECT * FROM bug WHERE id = ?", $_GET["id"]);
      // print_r($recordData);
      if (empty($recordData)) {
        $recordData = array("id" => "x", "product_name" => "-", "version" => "-", "hardware_type" => "-", "os" => "-", "frequency" => "-", "solution" => "-");
      }
    ?>
  <div>
    <form action="<?php echo htmlentities($_SERVER["PHP_SELF"]). "?id=" . $recordData["id"]?>" method="POST">
      <div>
        <label for="product">Product</label>
        <input type="text" id="product" name="product" value="<?php echo $recordData["product_name"] ?>">
      </div>
      <div>
        <label for="version">Version</label>
        <input type="text" id="version" name="version" value="<?php echo $recordData["version"] ?>">
      </div>
      <div>
        <label for="hardware">Hardware</label>
        <input type="text" id="hardware" name="hardware" value="<?php echo $recordData["hardware_type"] ?>">
      </div>
      <div>
        <label for="os">OS</label>
        <input type="text" id="os" name="os" value="<?php echo $recordData["os"] ?>">
      </div>
      <div>
        <label for="frequency">Frequency</label>
        <input type="text" id="frequency" name="frequency" value="<?php echo $recordData["frequency"] ?>">
      </div>
      <div>
        <label for="solution">Solution</label>
        <input type="text" id="solution" name="solution" value="<?php echo $recordData["solution"] ?>">
      </div>
      <div>
       <?php if ($recordData["id"] == "x") : ?>
        <input type="submit" name="submit" value="Submit" disabled>
        <?php else : ?>
        <input type="submit" name="submit" value="Submit">
        <?php endif ?>
      </div>
    </form>
  </div>
  <?php else : ?>
    <p> No ID has been filled in, please go back to the <a href="./Mainpage.php"> mainpage </a> </p>
  <?php endif; ?>
</body>

</html>

<?php 

  if (isset($_POST['submit'])){
    $values["product_name"] = !empty($_POST['product']) ? FILTER_INPUT(INPUT_POST, 'product', FILTER_SANITIZE_SPECIAL_CHARS) : false;
    $values["version"] = !empty($_POST['version']) ? FILTER_INPUT(INPUT_POST, 'version', FILTER_SANITIZE_SPECIAL_CHARS) : false;
    $values["hardware_type"] = !empty($_POST['hardware']) ? FILTER_INPUT(INPUT_POST, 'hardware', FILTER_SANITIZE_SPECIAL_CHARS) : false;
    $values["os"] =  !empty($_POST['os']) ? FILTER_INPUT(INPUT_POST, 'os', FILTER_SANITIZE_SPECIAL_CHARS) : false;
    $values["frequency"] = !empty($_POST['frequency']) ? FILTER_INPUT(INPUT_POST, 'frequency', FILTER_SANITIZE_SPECIAL_CHARS) : false;
    $values["solution"] = !empty($_POST['solution']) ? FILTER_INPUT(INPUT_POST, 'solution', FILTER_SANITIZE_SPECIAL_CHARS) : false;

    foreach ($values as $key => $value) {
      if (!$value){
        echo "<p> <strong>$key</strong> field has no value! Please fill in the value </p>";
      } 
    }
  
    if (!in_array(false, $values)){
      $values["id"] = $recordData["id"];
      executeQuery("UPDATE bug SET product_name=?, version=?, hardware_type=?, os=?, frequency=?, solution=? WHERE id = ?", $values, "update");
      header("Location: mainpage.php");
    } 
  }

?>